Daily Activities - 

Preface

Daily activities are intended to be associate-driven asynchronous activities for individuals to get practice with concepts covered in training. These activities are based off of those included in the [NEXT GEN JAVA AWS ANGULAR V3.2](https://app.revature.com/admin-v2/curriculum/view/628?isEditable=false) curriculum.

Weekly Breakdown

| Week 1 | 
| -----  |
| [Git Exercise](../week-1/git/README.md) |
| [Flow Control Activity](../week-1/control-flow/README.md) |
| [TDD Activity](../week-1/tdd/README.md) |
| [Exercise Implementing HashMap](../week-1/hashmap/README.md) |
| [Interview Questions](../week-1/interview-questions/README.md) | 
| [Tuesday Coding Challenge](../week-1/tuesday-coding-challenge/README.md) |
| [Wednesday Coding Challenge](../week-1/wednesday-coding-challenge/README.md) |
| [Thursday Coding Challenge](../week-1/thursday-coding-challenge/README.md) | 
| [Friday Live Coding Challenge #1](../week-1/friday-coding-challenge/README.md) | 


| Week 2 |
| --- | 
| Class Activity: Spin Up Postgres Container |
| QC Coding Challenge - Level 1 |
| Chinook Activity | 
| Wednesday SQL Query Challenge |
| Thursday SQL Query Challenge |
| Group Activity: Perform an Aggregation in a JDBC Query | 
| Build Demo JDBC Application with DAO |