## Exercise Using a HashMap

### Scope 

This exercise aims to give some first hand experience to associates using collections, including Maps.

### Example Activity

Example: 

Write a program to determine if two words are anagrams.

Associate Facing Requirements:

Implement a method which accepts two string arguments. The method should return true if the strings are anagrams of each other (contain the same letters the same number of times). The method should return false if the two strings are not anagrams.

Use any collection or logic that you'd like to accomplish this. However, if you don't use a HashMap to implement a solution, go back and refactor it to use one.

Example Solution:

https://gitlab.com/carolynrehm/java-competency-activities

---

Example:

Create a console based adventure game using a hashmap. The choices in the adventure game represent the key, and the result of that choice is represented by the value.

Associate Facing Requirements:

Example Solution:

https://gitlab.com/carolynrehm/java-competency-activities






