## Git Activity

### Scope 
The goal of the activity is to give associates some hands on experiencing performing the steps of a simple git workflow. Associates should use cli to perform commands such as clone, push, pull, add, and commit. Optionally, associates could work on a group activity where they can contribute to a shared repository to see the workflow in action in a collaborative environment.

### Example Activity

Example: 

Create a personal repository for taking notes.

Associate Facing Requirements:

```
## Git Exercise

Create a repository for you to keep track of notes during training. Begin by creating a personal remote repository on GitHub/GitLab. Make sure to initialize the repository with a generated README. Then clone your remote repository to your local environment. You should see a README file. 

Create a new file in your local repository. Add any notes that you took from today to that file. If you didn't take any notes, jot down some thoughts/reflections on what was covered today. 

After making those changes, commit them to your local repository and push the commit to your remote repository.

Feel free to work with others to accomplish this task, but everyone must create their own repository. You can reference some helpful git commands [here](https://education.github.com/git-cheat-sheet-education.pdf). When you've set up your repository and it contains your two commits (the initial commit with the readme, and your additional commit with your added content), post a link to it in [this excel sheet](link).
```

--- 

Example: 

Everyone adds their name to a file in a shared repository. This example is more challenging than the above and will require more collaboration. Expect to run into many merge conflicts; if you don't cover or talk about merge conflicts prior to this activity, this may not be the best activity to use.

Associate Facing Requirements:

```
## Git Exercise 

Today we're going to use git to have everyone in the cohort contribute to a file. Git is a tool for collaborative version control, so we'll have the opportunity to see the implications - and likely some of the challenges - that are associated with it.

Each person in the cohort will need to individually make a change to the code base. This means they'll need to clone the shared repository, make a change in their local repository, commit the change there, and push their change to the shared repository. This may require pulling more recent changes, and resolving merge conflicts. Work together as a team, collaborating with others in the cohort to resolve these challenges.

Make sure to send your github username so that your trainer can give you the necessary privileges to make changes in their repository.
```

