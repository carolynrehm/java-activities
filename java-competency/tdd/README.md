## TDD Activity

### Scope 

The goal of this activity is to give associates hands on experience with TDD. They should be responsible for writing JUnit tests based off of a set of functional requirements, and then implementing functionality such that the requirements are satisfied and the tests pass. They should see the idea of red/green/blue testing in action, writing tests that fail, and then seeing the same tests pass after the appropriate implementation is added. This activity should also give them experience debugging a failed test case.

### Example Activity

Example: 

Perform TDD with a couple of simple self contained methods. These methods should have straightforward inputs and outputs so that associates can see testing in an isolated environment and no mocking is needed.

Associate Facing Requirements:

Can build off of a preexisting example like [this one](https://gitlab.com/220214-java-training/training-repos/tdd-demo/-/tree/main/).


```markdown
## TDD Exercise

- use TDD to write tests to satisfy each requirement; write at least one JUnit test for each requirement before implementing the desired functionality of that requirement
- move through each requirement one by one, refactoring your method each time if need be to satisfy that requirement before moving on to the next
- think about testing for multiple cases/edge cases

1. fizzBuzz
- Write a “fizzBuzz” method that accepts a number as input and returns it as a String.
- For multiples of three return “Fizz” instead of the number
- For the multiples of five return “Buzz”
- For numbers that are multiples of both three and five return “FizzBuzz”.

1. findMax
- Write a method that takes in an int array, and returns the max value in an array. This can be included 
- Your method should be able to accommodate an array that contains only negative numbers
- Challenge Q: if the user attempts to use the FindMax method with a null input or an empty array, a custom exception should be thrown
```
