## Interview Questions

### Scope 

Provide associates with an idea of what type of interview questions might be asked surrounding the content from this week.

### Example Associate Content

- What is the difference between Agile and Waterfall methodologies?
- What are some benefits of using Agile for software development?
- Talk me through the steps of a git workflow.
- What is the difference between the JDK, JRE, and JVM?
- Walk me through the process of writing a JUnit test to test some functionality in your application?
- How can you handle exceptions in Java?
- What are some strategies you use to debug your code?
- What is the benefit of logging in your application?
- What is the difference between an interface and an abstract class?
- What does the static keyword mean in Java?
- Compare and contrast an ArrayList and LinkedList.
- What is HTTP?
- What are the different HTTP verbs used for?
- What do the different HTTP status codes indicate?
- How does Java manage memory relative to the stack and the heap?

